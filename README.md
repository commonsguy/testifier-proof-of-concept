# Testifier Proof-of-Concept

Testifier is a means of packaging select Android instrumented tests in an app,
so they can be run without an IDE or other developer tools. The vision is that
this app would be used to expand the scope of testing, including:

- Distributing the app to non-developer employees or group members who might
be willing to run it

- Distributing the app to "power users" who would be willing to run it

- Sending the app to users that are encountering specific compatibility problems
with your main app or library, to see if the test suite can give you more details
as to what is going wrong, for hardware that the user has and you do not

Testifier is "simply" a JUnit test runner, executing JUnit tests from
an app and displaying the results in a list. The "simply" part is that Android
instrumented tests are *so* not designed to run in a regular app, so Testifier
will need to do a bit of fancy footwork (mostly involving Mockito mocks/spies)
to maximize compatibility.

This repo represents a proof-of-concept of Testifier, created by Mark Murphy
of CommonsWare.

## Objectives

Testifier should be easy to integrate into an Android project, at least for
projects centered around a library. I hope that Testifier will be usable for
app-centered projects as well, though the integration process may get a bit more
gnarly.

Testifier should support a reasonable range of instrumented tests, though not
all of them. In particular, Testifier should support tests that help you determine
why your app or library works fine "in the lab" but is failing on hardware that
you do not have direct access to.

Testifier should not impose any limitations on the code that is being tested.
For example, if your app or library uses the NDK, Testifier should still run
tests that, via your Java/Kotlin code, exercises the NDK.

Testifier should work with "natural" instrumented tests as much as possible.
In other words, Testifier should not require you to rewrite your instrumented
tests to work, except perhaps for adding some annotations. Testifier should
work with tests written in Java or Kotlin.

Long-term, Testifier could have lots of interesting capabilities, such as:

- Integrated delivery of a test result report, so the user can click "send"
and send the report to you via email, direct `POST` to your Web server, etc.
That report would include stack traces for all failed tests, plus some
information about the test environment (e.g., device make/model).

- Convenient ways to indicate which instrumented tests should and should not
be included, so you can easily block tests that are not Testifier-friendly.

- Plugin APIs to support custom report contents, report delivery options, etc.

## About the Proof-of-Concept

The `library/` module represents the code under test and the tests themselves.
In other words, this is an ordinary Android module, though in this case it is
a library module, not an app module. The library has no code of significance,
though it does pull in [SQLCipher for Android](https://www.zetetic.net/sqlcipher/sqlcipher-for-android/)
as a dependency. Note that some of the tests fail &mdash; this is intentional,
as I need to confirm that Testifier could handle failing tests without crashing.

The `testifierLib/` module represents Testifier itself, at least at the level
of this proof of concept. Ordinarily, this would be an artifact that you would
pull in from a Maven repository, rather than be a module in the project itself.

The `testifierCustomApp/` module represents the project's Testifier instance.
This is the app module that creates the Testifier app to run the `library/`
tests. This module creates the app that you would distribute to your desired
testers.

## Integrating Testifier

These are the steps for integrating Testifier to a project, based on this
proof of concept. The hope is that while the details will change, the overall
level of complexity will not be a lot higher as Testifier evolves.

The instructions are for a project where the "real code" is a library module
(`library/` in the case of this project). While Testifier hopefully will support
integration with app modules sometime, the integration instructions will differ.

### Step #1: Create the App Module

In this project, that is `testifierCustomApp/`. You would create this as you
would any other app module (e.g., Android Studio new-module wizard).

### Step #2: Synchronize Relevant Settings

In principle, the Testifier custom app module should use the same `minSdkVersion`,
`targetSdkVersion`, etc. that your main code does. Whether you just manually
keep them in sync, leverage Gradle `ext` values to share constants between
the modules, or do something else, is up to you. This proof of concept largely
ignores this step.

### Step #3: Add Testifier

The Testifier custom app module needs an `implementation` dependency on the
Testifier library. In this proof of concept, that is a direct reference to the
associated `testifierLib/` module (`implementation project(":testifierLib")`).
Longer-term, this would be a reference to a standard Maven-style artifact.

### Step #4: Add the Production Code

The Testifier custom app module needs an `implementation` dependency pulling
in your production code and all of its dependencies. In the proof of concept,
this is just `implementation project(":library")`.

### Step #5: Add the Production Tests

The Testifier custom app module needs access to the instrumented tests of the
production library module. For a typical Android/Gradle project, that is a
matter of updating the custom app module's source sets to pull in the production
test code:

```gradle
sourceSets {
    main {
        java.srcDirs += ['../library/src/androidTest/java']
    }
}
```

Note that IDEs may not be the happiest with this sort of construction, so if
you are working inside the custom app module, do not be surprised if things
like code completion do not work all that well for the classes pulled in
via this `sourceSets` configuration.

### Step #6: Extend the `TestifierActivity`

When you created the Testifier custom app module, you probably got a starter
activity set up with the typical `MAIN`/`LAUNCHER` `<intent-filter>`.

If so, just change it to extend `TestifierActivity` and delete any code-generated
`onCreate()` or other functions that are in that starter activity. This is how
`MainActivity` in the proof of concept was set up.

If you did not get a starter activity, add an activity to the custom app module,
extending `TestifierActivity`, and add it to the custom app module's manifest
as a launcher activity.

### Step #7: Perform Any Necessary Initialization/Integration With Your Production Code

For example, if you need the app to do some work in a custom `Application`
subclass, you need to set that up. This may be less of an issue with libraries.

### Steps That Hopefully You Will Not Need in the Future

Right now, your activity will need to override a `getClassesToTest()` function
and return a list of `Class` objects representing the classes to test. Long-term,
this should be handled for you in many cases by an annotation processor, though
there will probably still be a hook for you to tailor things further from
Java/Kotlin.

Right now, your activity will need to request any runtime permissions that you
need the app using your library to request. Long-term, this can be handled
via introspection, using `PackageManager` to see what `dangerous` permissions
the custom app requests (pulled in from the library's manifest) and request
those permissions before running the tests.

### Step #8: Run the Testifier Custom App

At this point, you should be able to build and run the Testifier custom app,
which will run through your tests, showing you a summary of their results
and a list of each test's results. Also, failed tests have their stack traces
dumped to Logcat.

The `testifierCustomApp/` module in this project should be runnable in its
current state, so you can see what this looks like.

Note that the Testifier UI is just a stop-gap and will need more work.

## Fictionally-Asked Questions (FAQs)

Here are some questions that somebody might ask about Testifier, at least in theory.

### What Sorts of Tests Can Testifier Use?

It should support simple instrumented tests, ones where you need a `Context`
for testing but not a lot more.

It is *very* unlikely to support UiAutomator, as there is no way that an app
should be able to control another app that way, so some security issue will crop
up.

It is not designed for running unit tests. Unit tests in Android run on the JVM
and are there purely to exercise app logic. That app logic should not vary based
on Android version... and a unit test would not catch that anyway, since it does
not run on Android. In principle, unit tests could work, so long as you are not
relying on Robolectric (which is not designed to be used on Android).

### Why Is It Called "Testifier"?

Because I suck at naming things.
