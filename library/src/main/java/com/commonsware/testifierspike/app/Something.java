package com.commonsware.testifierspike.app;

public class Something {
  private String value;

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
