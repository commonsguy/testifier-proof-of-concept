/*
 Copyright (c) 2019 CommonsWare, LLC
 Licensed under the Apache License, Version 2.0 (the "License"); you may not
 use this file except in compliance with the License. You may obtain	a copy
 of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
 by applicable law or agreed to in writing, software distributed under the
 License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
 OF ANY KIND, either express or implied. See the License for the specific
 language governing permissions and limitations under the License.
 */

package com.commonsware.testifierspike.app

import androidx.test.platform.app.InstrumentationRegistry
import net.sqlcipher.database.SQLiteDatabase
import org.junit.rules.ExternalResource
import java.io.File

private const val DATABASE_NAME = "test.db"
private const val DATABASE_PASSWORD = "test"

class SimpleDbRule : ExternalResource() {
  lateinit var db: SQLiteDatabase
  private lateinit var databaseFile: File

  override fun before() {
    super.before()

    val appContext = InstrumentationRegistry.getInstrumentation().targetContext

    SQLiteDatabase.loadLibs(appContext)
    databaseFile = appContext.getDatabasePath(DATABASE_NAME)
    db = SQLiteDatabase.openOrCreateDatabase(
      databaseFile.path,
      DATABASE_PASSWORD,
      null,
      null
    )
  }

  override fun after() {
    db.close()
    databaseFile.delete()

    super.after()
  }
}