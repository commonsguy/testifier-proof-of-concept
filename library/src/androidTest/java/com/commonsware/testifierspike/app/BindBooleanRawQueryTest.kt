/*
 Copyright (c) 2019 CommonsWare, LLC
 Licensed under the Apache License, Version 2.0 (the "License"); you may not
 use this file except in compliance with the License. You may obtain	a copy
 of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
 by applicable law or agreed to in writing, software distributed under the
 License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
 OF ANY KIND, either express or implied. See the License for the specific
 language governing permissions and limitations under the License.
 */

package com.commonsware.testifierspike.app

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class BindBooleanRawQueryTest {
  @get:Rule
  val dbRule = SimpleDbRule()

  @Test
  fun testBind() {
    dbRule.db.execSQL("create table t1(a,b);")
    dbRule.db.execSQL(
      "insert into t1(a,b) values(?, ?);",
      arrayOf<Any>("one for the money", true)
    )

    val cursor =
      dbRule.db.rawQuery("select * from t1 where b = ?;", arrayOf<Any>(true))

    if (cursor != null) {
      if (cursor.moveToFirst()) {
        val a = cursor.getString(0)
        val b = cursor.getInt(1)
        cursor.close()
        assertEquals(a, "one for the money")
        assertEquals(b, 1)
      }
    }
  }
}