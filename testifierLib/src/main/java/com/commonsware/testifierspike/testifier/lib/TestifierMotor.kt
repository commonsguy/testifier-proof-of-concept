/*
 Copyright (c) 2019 CommonsWare, LLC
 Licensed under the Apache License, Version 2.0 (the "License"); you may not
 use this file except in compliance with the License. You may obtain	a copy
 of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
 by applicable law or agreed to in writing, software distributed under the
 License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
 OF ANY KIND, either express or implied. See the License for the specific
 language governing permissions and limitations under the License.
 */

package com.commonsware.testifierspike.testifier.lib

import android.app.Application
import android.app.Instrumentation
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.test.platform.app.InstrumentationRegistry
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.junit.runner.Description
import org.junit.runner.JUnitCore
import org.junit.runner.notification.Failure
import org.junit.runner.notification.RunListener
import org.mockito.Mockito
import java.util.*
import java.util.concurrent.atomic.AtomicReference
import kotlin.concurrent.thread

data class TestName(
  val testClassPackage: String,
  val testClassName: String,
  val testMethodName: String
)

sealed class TestResult(
  val test: TestName,
  val displayName: String
) {
  class Success(test: TestName, displayName: String) :
    TestResult(test, displayName)

  class Failure(test: TestName, displayName: String, val throwable: Throwable) :
    TestResult(test, displayName)

  class AssumptionFailure(
    test: TestName,
    displayName: String,
    val throwable: Throwable
  ) : TestResult(test, displayName)
}

data class TestifierViewState(
  val requestedTestCount: Int,
  val results: List<TestResult> = listOf()
)

class TestifierMotor(app: Application) : AndroidViewModel(app) {
  private val _states = MutableLiveData<TestifierViewState>().apply {
    value = TestifierViewState(0)
  }
  val states: LiveData<TestifierViewState> = _states
  private val junit = JUnitCore().apply {
    addListener(TestListener())
  }
  private val lastState =
    AtomicReference<TestifierViewState>().apply { set(_states.value!!) }

  init {
    val context: Context = getApplication()

    System.setProperty("org.mockito.android.target", context.cacheDir.path)

    val mock = mock<Instrumentation> {
      on { targetContext }.doReturn(context)
    }

    System.setProperty(
      "android.junit.runner",
      "androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner"
    )
    InstrumentationRegistry.registerInstance(mock, Bundle())
  }

  fun test(classes: List<Class<*>>) {
    val initial = TestifierViewState(classes.size)

    lastState.set(initial)
    _states.value = initial

    thread {
      classes.forEach {
        junit.run(it)
        Mockito.framework().clearInlineMocks()
      }
    }
  }

  private fun postFailure(failure: Failure) {
    synchronized(_states) {
      val current = lastState.get()
      val update = current.copy(
        results = current.results + TestResult.Failure(
          buildTestName(failure.description),
          failure.description.displayName,
          failure.exception
        )
      )

      lastState.set(update)
      _states.postValue(update)
    }
  }

  private fun postAssumptionFailure(failure: Failure) {
    synchronized(_states) {
      val current = lastState.get()
      val update = current.copy(
        results = current.results + TestResult.AssumptionFailure(
          buildTestName(failure.description),
          failure.description.displayName,
          failure.exception
        )
      )

      lastState.set(update)
      _states.postValue(update)
    }
  }

  private fun postSuccess(description: Description) {
    synchronized(_states) {
      val current = lastState.get()
      val update = current.copy(
        results = current.results + TestResult.Success(
          buildTestName(description),
          description.displayName
        )
      )

      lastState.set(update)
      _states.postValue(update)
    }
  }

  private fun buildTestName(description: Description) =
    TestName(
      description.testClass.`package`?.name.orEmpty(),
      description.testClass.simpleName,
      description.methodName
    )

  inner class TestListener : RunListener() {
    private val outstandingTests =
      Collections.synchronizedSet(mutableSetOf<Description>())

    override fun testStarted(description: Description) {
      if (description.isTest) {
        outstandingTests += description
      }
    }

    override fun testFinished(description: Description) {
      if (outstandingTests.contains(description)) {
        outstandingTests -= description

        postSuccess(description)
      }
    }

    override fun testAssumptionFailure(failure: Failure) {
      if (failure.description.isTest) {
        outstandingTests -= failure.description
        postAssumptionFailure(failure)
        Log.e("Testifier", "Test Assumption Failure", failure.exception)
      }
    }

    override fun testFailure(failure: Failure) {
      if (failure.description.isTest) {
        outstandingTests -= failure.description
        postFailure(failure)
        Log.e("Testifier", "Test Exception", failure.exception)
      }
    }
  }
}
