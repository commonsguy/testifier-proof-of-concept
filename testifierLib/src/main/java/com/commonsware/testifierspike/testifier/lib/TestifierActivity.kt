/*
 Copyright (c) 2019 CommonsWare, LLC
 Licensed under the Apache License, Version 2.0 (the "License"); you may not
 use this file except in compliance with the License. You may obtain	a copy
 of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
 by applicable law or agreed to in writing, software distributed under the
 License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
 OF ANY KIND, either express or implied. See the License for the specific
 language governing permissions and limitations under the License.
 */

package com.commonsware.testifierspike.testifier.lib

import android.annotation.SuppressLint
import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import androidx.recyclerview.widget.*
import kotlinx.android.synthetic.main.testifier_main.*

abstract class TestifierActivity : AppCompatActivity() {
  abstract fun getClassesToTest(): List<Class<*>>

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.testifier_main)

    val manager = LinearLayoutManager(this)
    val resultsAdapter = ResultsAdapter(layoutInflater, resources)

    testResults.apply {
      layoutManager = manager
      addItemDecoration(
        DividerItemDecoration(
          this@TestifierActivity,
          manager.orientation
        )
      )
      adapter = resultsAdapter
    }

    val motor = ViewModelProviders.of(this)[TestifierMotor::class.java]

    motor.states.observe(this) { state ->
      val results = state.results

      totalCount.text = results.size.toString()
      successCount.text = results.count { it is TestResult.Success }.toString()
      failureCount.text = results.count { it is TestResult.Failure }.toString()
      skippedCount.text = results.count { it is TestResult.AssumptionFailure }.toString()

      percent.text = if (state.requestedTestCount > 0) {
        ((state.results.size * 100) / state.requestedTestCount).toString() + "%"
      } else ""

      resultsAdapter.submitList(results)
    }

    motor.test(getClassesToTest())
  }
}

class ResultsAdapter(
  private val inflater: LayoutInflater,
  private val resources: Resources
) : ListAdapter<TestResult, RowHolder>(TestResultDiffer) {
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = RowHolder(
    inflater.inflate(R.layout.testifier_row, parent, false),
    resources
  )

  override fun onBindViewHolder(holder: RowHolder, position: Int) {
    holder.bind(getItem(position))
  }
}

class RowHolder(root: View, private val resources: Resources) :
  RecyclerView.ViewHolder(root) {
  private val className = root.findViewById<TextView>(R.id.className)
  private val methodName = root.findViewById<TextView>(R.id.methodName)
  private val resultLabel = root.findViewById<TextView>(R.id.result)

  @SuppressLint("SetTextI18n")
  fun bind(result: TestResult) {
    className.text = result.test.testClassName
    methodName.text = "${result.test.testMethodName}()"

    when (result) {
      is TestResult.Success -> {
        resultLabel.setText(R.string.testifier_label_success)
        resultLabel.setTextColor(
          ResourcesCompat.getColor(
            resources,
            R.color.testifierSuccess,
            null
          )
        )
      }
      is TestResult.Failure -> {
        resultLabel.setText(R.string.testifier_label_failed)
        resultLabel.setTextColor(
          ResourcesCompat.getColor(
            resources,
            R.color.testifierFailure,
            null
          )
        )
      }
      is TestResult.AssumptionFailure -> {
        resultLabel.setText(R.string.testifier_label_skipped)
        resultLabel.setTextColor(
          ResourcesCompat.getColor(
            resources,
            R.color.testifierSkipped,
            null
          )
        )
      }
    }
  }
}

object TestResultDiffer : DiffUtil.ItemCallback<TestResult>() {
  override fun areItemsTheSame(
    oldItem: TestResult,
    newItem: TestResult
  ) = oldItem === newItem

  override fun areContentsTheSame(
    oldItem: TestResult,
    newItem: TestResult
  ) = oldItem.javaClass.canonicalName == newItem.javaClass.canonicalName &&
      oldItem.displayName == newItem.displayName &&
      oldItem.test == newItem.test
}