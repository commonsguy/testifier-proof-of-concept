/*
 Copyright (c) 2019 CommonsWare, LLC
 Licensed under the Apache License, Version 2.0 (the "License"); you may not
 use this file except in compliance with the License. You may obtain	a copy
 of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
 by applicable law or agreed to in writing, software distributed under the
 License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
 OF ANY KIND, either express or implied. See the License for the specific
 language governing permissions and limitations under the License.
 */

package com.commonsware.testifierspike.testifier

import com.commonsware.testifierspike.app.*
import com.commonsware.testifierspike.testifier.lib.TestifierActivity

class MainActivity : TestifierActivity() {
  override fun getClassesToTest() = listOf(
    BeginTransactionTest::class.java,
    BeginTransactionInJavaTest::class.java,
    BindBooleanRawQueryTest::class.java,
    CheckIsDatabaseIntegrityOkTest::class.java,
    SlowTest::class.java,
    FailedAssertionTest::class.java,
    ExceptionTest::class.java,
    FailedAssumptionTest::class.java,
    SomethingTest::class.java
  )
}
